const functions = require('firebase-functions');
const admin = require('firebase-admin');

admin.initializeApp();

// to serve locally
// var serviceAccount = require("../EasyPark-6b71ee17f5f3.json");

// admin.initializeApp({
//     credential: admin.credential.cert(serviceAccount),
//     databaseURL: "https://easypark-65a86.firebaseio.com"
// });

exports.askCurrentCost = functions.https.onCall(async (data, context) => {
    let startDate;
    return admin.firestore().collection('Entry').doc(data.entryId).get().then((entry) => {
        startDate = entry.data().startDateTime.toDate();
        return entry.data().parkingId.get()
    }).then((parking) => {
        const minutes = Math.round(Math.abs(new Date() - startDate) / 1000 / 60);
        const price = parking.data().hourRate * Math.ceil(minutes / 60);
        return { price, minutes };
    }).catch(() => {
        return 'There was an error!';
    })
});

exports.newEntry = functions.https.onCall(async (data, context) => {
    const parkingId = data.parkingId;

    const parkingDocRef = admin.firestore().collection('Parking').doc(parkingId);

    return parkingDocRef.get().then((parkingSnapshot) => {
        const parking = parkingSnapshot.data()
        const newEntry = {
            status: 'WAITING',
            parkingId: parkingDocRef,
            parkingName: parking.displayName
        };
        return admin.firestore().collection('Entry').add(newEntry);
    })
        .then((docRef) => {
            return { documentId: docRef.id };
        })
        .catch(() => {
            return 'There was an error!';
        });
});
