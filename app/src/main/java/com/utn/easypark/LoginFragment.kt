package com.utn.easypark

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.navigation.findNavController


class LoginFragment : Fragment() {
    private lateinit var v: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.fragment_login, container, false)
        v.findViewById<TextView>(R.id.create_account_link).setOnClickListener {
            v.findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToCreateAccountFragment())
        }
        return v
    }

    override fun onStart() {
        super.onStart()
        v.findViewById<Button>(R.id.button).setOnClickListener{
            startActivity(Intent(activity, MainActivity::class.java))
            requireActivity().finish()
        }
    }
}
