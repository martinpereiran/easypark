package com.utn.easypark

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreException
import com.utn.easypark.database.models.Entry

class HistoryFragment : Fragment() {
    lateinit var v: View
    lateinit var recyclerView : RecyclerView
    lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: FirestoreRecyclerAdapter<Entry, EntryHolder>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.fragment_history, container, false)
        recyclerView = v.findViewById(R.id.entries_history)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(context)
        return v
    }

    override fun onStart() {
        super.onStart()
        val query = FirebaseFirestore.getInstance().collection("Entry").whereEqualTo("status", Entry.Statuses.finished)
        val options = FirestoreRecyclerOptions.Builder<Entry>()
            .setQuery(query, Entry::class.java)
            .build()
        adapter = object :
            FirestoreRecyclerAdapter<Entry, EntryHolder>(options) {
            override fun onError(e: FirebaseFirestoreException) {
                super.onError(e)
            }
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EntryHolder{
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.entry_list_item, parent, false)
                return EntryHolder(view)
            }

            override fun onBindViewHolder(holder: EntryHolder, position: Int, model: Entry) {
                holder.setEntry(model)
            }

            override fun onDataChanged() {
                super.onDataChanged()
            }
        }
        adapter.startListening()
        recyclerView.adapter = adapter
    }

}
