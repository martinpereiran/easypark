package com.utn.easypark

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.findNavController

class CreateAccountFragment : Fragment() {
    private lateinit var v: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.fragment_create_account, container, false)
        v.findViewById<TextView>(R.id.login_link).setOnClickListener{
            v.findNavController().navigate(CreateAccountFragmentDirections.actionCreateAccountFragmentToLoginFragment())
        }
        return v
    }

}
