package com.utn.easypark

import android.Manifest
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.google.android.gms.tasks.Tasks
import com.google.firebase.firestore.FirebaseFirestore
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.utn.easypark.database.DbService
import com.utn.easypark.database.models.Entry
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import me.dm7.barcodescanner.zxing.ZXingScannerView
import java.lang.Exception

class ScannerFragment : Fragment() {
    private lateinit var v: View
    private lateinit var zxscan: ZXingScannerView
    private lateinit var scanViewModel: ScanViewModel

    override fun onPause() {
        super.onPause()
        zxscan.stopCamera()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.fragment_scanner, container, false)
        scanViewModel = ViewModelProvider(requireActivity()).get(ScanViewModel::class.java)
        zxscan = v.findViewById(R.id.zxscan)
        Dexter.withActivity(requireActivity())
            .withPermission(Manifest.permission.CAMERA)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                    zxscan.setResultHandler {
                        if (it != null) {
                            zxscan.stopCamera()
                            val parentJob = Job()
                            val scope = CoroutineScope(Dispatchers.Default + parentJob)
                            scope.launch {
                                try {
                                    val entryDocRef = DbService.getEntryByDocumentId(it.toString())
                                    var entry = Tasks.await(entryDocRef.get()).toObject(Entry::class.java)
                                    if(entry!!.status != Entry.Statuses.waiting){
                                        throw Exception()
                                    }
                                    entry.status = Entry.Statuses.active
                                    DbService.updateEntry(entry)
                                        .addOnSuccessListener {
                                            scanViewModel.setEntry(entryDocRef)
                                            scanViewModel.scanError = false
                                            val action =
                                                ScannerFragmentDirections.actionScannerFragmentToTabsFragment()
                                            v.findNavController().navigate(action)
                                        }
                                        .addOnFailureListener{
                                            throw Exception()
                                        }
                                }catch (ex: Exception){
                                   scanViewModel.scanError = true
                                    val action =
                                        ScannerFragmentDirections.actionScannerFragmentToTabsFragment()
                                    v.findNavController().navigate(action)
                                }
                            }
                        }
                    }
                    zxscan.startCamera()
                }

                override fun onPermissionRationaleShouldBeShown(
                    permission: PermissionRequest?,
                    token: PermissionToken?
                ) {
                }

                override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                }

            }).check()
        return v
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
}
