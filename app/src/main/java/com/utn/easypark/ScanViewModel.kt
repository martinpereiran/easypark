package com.utn.easypark

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.DocumentSnapshot
import com.utn.easypark.database.models.Entry

class ScanViewModel(val app: Application) : AndroidViewModel(app) {
    private val scannerStatus = MutableLiveData<Boolean>()
    private val currentEntry = MutableLiveData<DocumentReference>(null)
    var scanError = false

    fun setEntry(entry: DocumentReference){
        currentEntry.postValue(entry)
    }

    fun getEntry(): LiveData<DocumentReference>{
        return currentEntry
    }

    fun setScannerStatus(status: Boolean) {
        scannerStatus.postValue(status)
    }

    fun getScannerStatus(): LiveData<Boolean> {
        return scannerStatus
    }
}