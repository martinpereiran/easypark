package com.utn.easypark

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.google.firebase.functions.FirebaseFunctions
import com.utn.easypark.database.DbService
import com.utn.easypark.services.CurrentPrice
import com.utn.easypark.services.FirebaseFunctionsService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class PayDialogFragment : DialogFragment() {
    private lateinit var v: View
    private lateinit var scanViewModel: ScanViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.fragment_pay_dialog, container, false)
        scanViewModel = ViewModelProvider(requireActivity()).get(ScanViewModel::class.java)
        val entryId = scanViewModel.getEntry().value!!.id
        var price: Float? = null
        FirebaseFunctionsService.getCurrentPrice(entryId)
            .addOnSuccessListener {
                val priceResult = CurrentPrice(it.data as Map<String, String>)
                price = priceResult.price
                val timeText = v.findViewById<TextView>(R.id.time_passed)
                timeText.visibility = View.VISIBLE
                timeText.text = v.context.getString(
                    R.string.time_passed,
                    (priceResult.minutes / 60) as Int,
                    priceResult.minutes % 60
                )
                val priceText = v.findViewById<TextView>(R.id.current_price)
                priceText.visibility = View.VISIBLE
                priceText.text = v.context.getString(R.string.stay_cost, priceResult.price)
                val payButton = v.findViewById<Button>(R.id.pay_button)
                payButton.visibility = View.VISIBLE
                payButton.setOnClickListener {
                    val payingSpinner = v.findViewById<ProgressBar>(R.id.paying_spinner)
                    payingSpinner.visibility = View.VISIBLE
                    payButton.visibility = View.INVISIBLE
                    val parentJob = Job()
                    val scope = CoroutineScope(Dispatchers.Default + parentJob)
                    scope.launch {
                        DbService.markEntryPayed(entryId, price!!).addOnSuccessListener {
                            dismiss()
                        }
                    }
                }
                v.findViewById<ProgressBar>(R.id.loadingPrice).visibility = View.INVISIBLE
            }
        return v
    }

}
