package com.utn.easypark.services

import com.google.android.gms.tasks.Task
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.functions.HttpsCallableResult

object FirebaseFunctionsService {
    fun getCurrentPrice(id: String): Task<HttpsCallableResult> {
        val params = hashMapOf(
            "entryId" to id
        )
        return FirebaseFunctions.getInstance().getHttpsCallable("askCurrentCost").call(params)
    }
}

class CurrentPrice(private val map: Map<String, Any>){
    val minutes:Int by map
    val price:Float by map
}