package com.utn.easypark

import android.content.DialogInterface
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.tasks.Tasks
import com.google.firebase.firestore.DocumentReference
import com.google.zxing.BarcodeFormat
import com.google.zxing.qrcode.QRCodeWriter
import com.utn.easypark.database.DbService
import com.utn.easypark.database.models.Entry
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat

class NewTicketFragment : Fragment() {
    private lateinit var v: View
    private lateinit var scanViewModel: ScanViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.fragment_new_ticket, container, false)
        scanViewModel = ViewModelProvider(requireActivity()).get(ScanViewModel::class.java)
        return v
    }

    override fun onStart() {
        super.onStart()
        val parentJob = Job()
        var entry = scanViewModel.getEntry().value
        when {
            entry == null -> {
                val scopeIO = CoroutineScope(Dispatchers.IO + parentJob)
                val scopeMain = CoroutineScope(Dispatchers.Main + parentJob)
                scopeIO.launch {
                    val entriesSnapshot = DbService.hasPendingEntry()
                    if (entriesSnapshot.isEmpty) {
                        scopeMain.launch {
                            setCurrentLayout(scanNew)
                        }
                    } else {
                        val entryObject = entriesSnapshot.documents[0].toObject(Entry::class.java)!!
                        entry = DbService.getEntryByDocumentId(entryObject.id)
                        scanViewModel.setEntry(entry!!)
                        scopeMain.launch {
                            setDocumentBind(entry!!)
                        }
                    }
                }
            }
            scanViewModel.scanError -> setCurrentLayout(error)
            else -> {
                setDocumentBind(entry!!)
            }
        }
    }

    private fun setDocumentBind(entryDocument: DocumentReference) {
        entryDocument.addSnapshotListener { snapshot, _ ->
            try {
                val entry = snapshot!!.toObject(Entry::class.java)!!
                when (entry.status) {
                    Entry.Statuses.active -> {
                        setCurrentLayout(current)
                        v.findViewById<TextView>(R.id.enjoy_stay_text).text =
                            getString(R.string.enjoy_your_stay, entry.parkingName)
                        val simpleDateFormat = SimpleDateFormat("hh:mm")
                        v.findViewById<TextView>(R.id.entry_time_text).text =
                            getString(R.string.entry_time, simpleDateFormat.format(entry.startDateTime))
                        v.findViewById<Button>(R.id.button).setOnClickListener {
                            val payDialog = PayDialogFragment()
                            payDialog.show(childFragmentManager, "payDialog")
                        }
                    }
                    Entry.Statuses.paid -> {
                        val content = entry.id
                        val writer = QRCodeWriter()
                        val bitMatrix = writer.encode(content, BarcodeFormat.QR_CODE, 512, 512)
                        val width = bitMatrix.width
                        val height = bitMatrix.height
                        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)
                        for (x in 0 until width) {
                            for (y in 0 until height) {
                                bitmap.setPixel(
                                    x,
                                    y,
                                    if (bitMatrix.get(x, y)) Color.BLACK else Color.WHITE
                                )
                            }
                        }
                        v.findViewById<ImageView>(R.id.qr_code).setImageBitmap(bitmap)
                        setCurrentLayout(qr)
                    }
                    Entry.Statuses.finished -> {
                        setCurrentLayout(scanNew)
                    }
                }
            } catch (e: Exception) {
                setCurrentLayout(error)
            }
        }
    }

    private fun setCurrentLayout(index: Int) {
        v.findViewById<View>(R.id.loading_layout).visibility = View.INVISIBLE
        v.findViewById<View>(R.id.start_layout).visibility = View.INVISIBLE
        v.findViewById<View>(R.id.qr_layout).visibility = View.INVISIBLE
        v.findViewById<View>(R.id.error_layout).visibility = View.INVISIBLE
        v.findViewById<View>(R.id.current_layout).visibility = View.INVISIBLE
        when (index) {
            loading -> v.findViewById<View>(R.id.loading_layout).visibility =
                View.VISIBLE
            scanNew -> {
                val view = v.findViewById<View>(R.id.start_layout)
                view.visibility =
                    View.VISIBLE
                view.setOnClickListener {
                    scanViewModel.setScannerStatus(true)
                }
            }
            qr -> v.findViewById<View>(R.id.qr_layout).visibility =
                View.VISIBLE
            error -> {
                val view = v.findViewById<View>(R.id.error_layout)
                view.visibility = View.VISIBLE
                view.setOnClickListener {
                    scanViewModel.setScannerStatus(true)
                }
            }
            current -> v.findViewById<View>(R.id.current_layout).visibility =
                View.VISIBLE
        }
    }

    companion object CurrentLayout {
        const val loading = 1
        const val scanNew = 2
        const val error = 3
        const val current = 4
        const val qr = 5
    }
}
