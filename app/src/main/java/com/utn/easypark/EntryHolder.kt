package com.utn.easypark

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.utn.easypark.database.models.Entry
import java.text.SimpleDateFormat

class EntryHolder(v: View): RecyclerView.ViewHolder(v) {
    private var view: View = v

    fun setEntry(entry: Entry){
        view.findViewById<TextView>(R.id.history_place).text = entry.parkingName
        val simpleDateFormat = SimpleDateFormat("dd/MM//yyyy")
        view.findViewById<TextView>(R.id.history_date).text = simpleDateFormat.format(entry.startDateTime)
        view.findViewById<TextView>(R.id.history_price).text = view.context.getString(R.string.price, entry.amount)
    }
}