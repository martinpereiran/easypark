package com.utn.easypark.database

import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.firebase.Timestamp
import com.google.firebase.firestore.*
import com.utn.easypark.database.models.Entry
import java.util.*

object DbService {
    private val db = FirebaseFirestore.getInstance()

    // looks for an Entry with the provided Id, if no Id found, an exception will raise
    suspend fun getEntryByDocumentId(id: String): DocumentReference {
        return db.collection("Entry").document(id)
    }

    suspend fun markEntryPayed(id: String, price: Float): Task<Void> {
        val items = mapOf<String, Any>(
            "status" to Entry.Statuses.paid,
            "endDateTime" to FieldValue.serverTimestamp(),
            "amount" to price
        )
        return db.collection("Entry").document(id).update(items)
    }

    suspend fun updateEntry(entry: Entry): Task<Void> {
        return db.collection("Entry").document(entry.id).set(entry)
    }

    suspend fun hasPendingEntry(): QuerySnapshot {
        return Tasks.await(
            db.collection("Entry")
                .whereIn("status", listOf(Entry.Statuses.active, Entry.Statuses.paid))
                .get()
        )
    }
}