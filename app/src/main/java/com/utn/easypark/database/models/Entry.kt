package com.utn.easypark.database.models

import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.ServerTimestamp
import java.time.temporal.TemporalAmount
import java.util.*

class Entry(){
    @DocumentId
    lateinit var id: String
    lateinit var parkingId: DocumentReference
    lateinit var parkingName: String
    lateinit var status: String
    @ServerTimestamp
    var startDateTime: Date? = null
    var amount: Float? = null

    object Statuses{
        const val waiting = "WAITING"
        const val active = "ACTIVE"
        const val paid = "PAID"
        const val finished = "FINISHED"
        const val error = "ERROR"
    }
}